# Introduction to Vue.js

Live at: [vue-presentation.surge.sh](http://vue-presentation.surge.sh)

## Setup

- Run `yarn`

### Scripts

 Command  | Description
----------|---------
 `build`  | create index.html from index.md
 `offline`| create index.offline.html
 `watch`  | rebuild on index.md change
 `serve`  | start live-server
 `expose` | deploy to [surge.sh](https://surge.sh)
 `dev`    | development mode: `watch` + `serve`

### Preview

![](images/slides.png)