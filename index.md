

![Logo](https://vuejs.org/images/logo.png)
# Introduction to Vue.js

---

![](images/house.jpg)

<notes>
How I feel about Connect
- We are not good at frontend - hence project became big and scary
- Time to rise awareness
</notes>

---

```impact
It's time to
rise
awareness
```

---

I don't feel like our future is bright with Angular...

![](images/happy-tree-friends.jpg)

---

- ① What is it?
- ② Should I try it?
- ③ OK. Show me!

Slides: [jsek-slides/vue-intro](https://gitlab.com/jsek-slides/vue-intro)

Demo: [j-sek/vue-basics-demo](https://github.com/J-Sek/vue-basics-demo)

---

<p class="q"><span class="circle">1</span></p>

---

||
-|-
Who is an author? | _**Evan You**_
Who backs it up? | Community
How much does it weight? | 20KB (gzipped)

<small>
Check: [OpenCollective](https://opencollective.com/vuejs) | [Patreon](https://www.patreon.com/evanyou)
</small>

---

![](images/evan-you.jpg)

---

### Resources

GitHub: [vuejs/vue](https://github.com/vuejs/vue/graphs/contributors)

Docs: [vuejs.org](https://vuejs.org/v2/guide/)

Libs: [curated.vuejs.org](https://curated.vuejs.org/module/github_com)

CheatSheets: [[1]](https://vuejs-tips.github.io/cheatsheet/), [[2]](http://codepop.com/Vue-Essentials-Cheat-Sheet.pdf), [[3]](https://devhints.io/vue)

<notes>
- Who is the main contributor?
- Which browsers are supported?
- How many repositories are directly related?

- Cheatsheet is similar to https://oscarotero.com/jquery/

- PDF by Gregg Pollack (creator of CodeSchool)
</notes>

---

<p class="q"><span class="circle">2</span></p>

---

<p class="q"><span class="circle">A</span>ngular</p><br/>
<p class="q"><span class="circle">R</span>eact</p><br/>
<p class="q"><span class="circle">V</span>ue.js</p>

<notes>
Actually React is leading the pack
Vue.js however surprised everyone by getting here in just 1 year
</notes>

---

![](images/coke-vs-pepsi.jpg)

---

```grid(1:1,#c3002f,bold)
Angular
```

---

## It was sad to see Angular&nbsp;2+ being unwanted child

<br>
<br>
<br>

- [Angular - Just say no](http://bit.ly/angular-just-say-no)
- [Why Angular 2/4 Is Too Little, Too Late](https://medium.com/@chriscordle/why-angular-2-4-is-too-little-too-late-ea86d7fa0bae)

---

### I guess I know why

- Webpack sucks
- Bundle is too heavy
- Too much complexity
- CLI saves my nerves but... I shouldn't need it
- I expected more from DevTools
- TypeScript sucks

<i></i>

- It's no fun :(

<notes>
Vue.js does have CLI too, but only to generate projects. Not for adding components.
</notes>

---

### Things I can't stand

<br>

- Requires serious expertise to setup a project
- Too much effort to get simple stuff done
- Requires long training
- Lots of parts you don't need
- Restrictive 🠚 lots of errors during development

---

![](images/restrictions.jpg)

<notes>
- This is how sociopaths are born
</notes>

---

```impact
Are you
emotionally attached
to your framework?
```

<notes>
- Probably yes, but you should not
</notes>

---

<small>Things that just didn't work out</small>

### Antipatterns of Angular

- 2-way binding
- Versioning (toxic)

<hr>

<small>...for me</small>

- Dependency injection
- TypeScript

<notes>
- I used to like 2-way binding - just worked... but debugging was dreadful
  - + using "$scope.$apply()" caused issues

- People are furious about versioning - major updates too often
  - RxJS - not SemVer

- I thought DI and types will solve large-codebase-problems
  - It turned out they does not fit to JavaScript
    - Cost time and nerves
</notes>

---

![](images/sax.gif)

<notes>
Experience with Coolkit + Cristal
(TypeScript does not help)
</notes>

---

```grid(1:1,#18bcee,bold)
React.js
```

---

### Why is *__ReactJS__* so popular?

- Performance
- Readability (Components)
- Predictability (Flux, Redux)
- [DevTools](https://github.com/facebook/react-devtools)

<hr>

<small>...for me</small>

- We waited too long for Angular 2
- ...which was a total rewrite
- \+ Hype

---

> But I didn't even have a chance to touch _React_...

---

#### You might not need to...

Read:<br>
[7 Strengths of #ReactJS Every Programmer Should Know About](https://blog.reactiveconf.com/7-strengths-of-reactjs-every-programmer-should-know-about-6a5f3a69a861)

<small>And then... skip it</small>

---

### My problems with React

- Hype
- You need to dive in
- A bit hard to get started
- Router changed too many times
- JSX

---

![](images/scr/jsx.png)

---

```grid(1:1,transparent,funky,bold)
Vue.js
```

---

### Frameworks leaderboard

![](images/scr/stars-angularjs.png)
![](images/scr/stars-angular.png)
![](images/scr/stars-react.png)
![](images/scr/stars-vue.png)

---

"NPM downloads" statistics

<div style="background: #fff">
    [![](images/npm-downloads.png)](http://www.npmtrends.com/@angular/core-vs-react-vs-vue)
</div>

---

<div style="background: #fff">
    [![](images/survey-results.png)](https://stateofjs.com/2017/front-end/results/)
</div>

---

### Vue.js strengths

- focus on developers
- simplicity
- tooling

---

### Why is *__Vue.js__* so popular?

- Templates resemble Angular.js 1.x
- Just... HTML + JavaScript + CSS
- Server-Side Rendering (SEO)
- Documentation
- Ecosystem

<notes>
All of them have docs.
Vuejs.org is however significantly more useful.
</notes>

---

![](images/ecosystem.png)

---

<iframe height='550' scrolling='no' title='Vue.js Ecosystem by Zircle' src='//codepen.io/zircle/embed/preview/LeqKGK/?height=700&theme-id=dark&default-tab=result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/zircle/pen/LeqKGK/'>Vue.js Ecosystem by Zircle</a> by Juan Martin (<a href='https://codepen.io/zircle'>@zircle</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>

---

### What are people saying?

> **React** - For those who love massive ecosystems and more flexibility.

> **Angular** - for TypeScript lovers. Requires serious expertise.

> **Vue** is relatively simple to pick up. Will become new jQuery.

---

#### Is it slowing you down?

---

![bg:](images/slime.jpg)

---

```impact,narrow
Focus on
the problem to solve
not fighting with framework
```

<notes>
My point of vue :)
</notes>

---

<p class="q"><span class="circle">3</span></p>

---

```impact
Vue.js
development
walkthough
```

---

![bg:demo](images/wallhaven-529853.jpg)

## Demo

---

<!-- For print -->
<!-- link to: https://youtu.be/p1iLqZnZPdo -->
<!-- YT icon -->
<!-- Why 43% of Front-End Developers want to learn Vue.js  -->

<iframe style='width: 100%;' height="700" src="https://www.youtube.com/embed/p1iLqZnZPdo?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

---

### Frontend only

```tree
/
├───/src
│   └───/{...}
│
├───/index.html
└───/package.json
```

---

```html
<!doctype html>
<html>
<body>
    <div id="app">
        <h1>{{ title }}</h1>
    </div>

    <script src='https://unpkg.com/vue'></script>
    <script>
        new Vue({
            el: '#app',
            data: {
                title: 'Hello Vue.js'
            },
        })
    </script>
</body>
</html>
```

<hr>

Can you believe this? No bloody webpack. Yey :)

---

![](images/bugs.jpg)

---

```grid(1:2,#00897b:#7cb342,bold)
less code | less bugs
```

--- 

```js
var EnvironmentLabel = Vue.component('environment-label', {
    template: `
        <p v-if='isLocal' class="dev">Localhost</p>
        <p v-else class="prod">Hostname: {{ hostname }}</p>
    `,
    data() {
        return {
            isLocal: false,
            hostname: '',
        }
    },
    mounted() {
        this.isLocal = /(127\.0\.0\.1)|(localhost)/.test(location.host);
        this.hostname = location.hostname;
    },
})
```
<hr>

Our first component. Now let's use it.

---

```grid(3:2,#1da06e:#183243,gap:.3em)
props | data | template
mounted | methods | watch
```

<notes>
Slots, Filters, Events, Routing, Inheritance, async data
</notes>

---

### Template directives

```grid(3:4,#597:->:#374,gap:.3em)
    v-text
    v-html

    v-show
    v-if
    v-else
    v-else-if
    
    v-for
    
    v-on
    v-once
    
    v-bind
    v-model
    
    v-cloak
```

<notes>
[](https://vuejs.org/v2/api/#Directives)
</notes>

---

```grid(2:1,#1da06e:#183243,gap:.1em)
v-bind: 🠚 : | v-on: 🠚 @
```

---

<iframe height='550' scrolling='no' title='Vue.js + Chart.js' src='//codepen.io/jsek/embed/preview/rvPwVR/?height=550&theme-id=dark&default-tab=result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/jsek/pen/rvPwVR?editors=1010'>Vue.js + Chart.js</a> by JSek (<a href='https://codepen.io/jsek'>@jsek</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>

<notes>
- Component: props, template, mounted()
- Wrapping 3rd-party library
- How these libs are attached?
- We can put expressions in binding
- .card(v-for='i in 3')

Summary - Vue.js recommended for prototyping
</notes>

---

<iframe height='550' scrolling='no' title='Vue.js + Chart.js (with settings)' src='//codepen.io/jsek/embed/preview/BxMZLm/?height=700&theme-id=dark&default-tab=result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/jsek/pen/BxMZLm?editors=1010'>Vue.js + Chart.js (with settings)</a> by JSek (<a href='https://codepen.io/jsek'>@jsek</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>

<notes>
- Input binding: lazy, parsing
- Component: data, watch

Summary - Vue.js recommended forms bindingW
</notes>

---

<iframe height='550' scrolling='no' title='Vue.js + Chart.js (with fetch)' src='//codepen.io/jsek/embed/preview/aGXyOB/?height=700&theme-id=dark&default-tab=result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/jsek/pen/aGXyOB?editors=1010'>Vue.js + Chart.js (with fetch)</a> by JSek (<a href='https://codepen.io/jsek'>@jsek</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>

<notes>
- Loading data: axios or fetch, parallel
- Loading indicator
- If component would be loaded (v-show), it would require "watch: { data: ... }"
- Switch to bar chart

Summary - Vue.js recommended for binding async data
</notes>

---

<iframe height='800' scrolling='no' title='Vue JS CRUD example' src='//codepen.io/Lewitje/embed/preview/bLZpgR/?height=700&theme-id=dark&default-tab=result&embed-version=2' frameborder='no' allowtransparency='true' allowfullscreen='true' style='width: 100%;'>See the Pen <a href='https://codepen.io/Lewitje/pen/bLZpgR/'>Vue JS CRUD example</a> by Lewi Hussey (<a href='https://codepen.io/Lewitje'>@Lewitje</a>) on <a href='https://codepen.io'>CodePen</a>.
</iframe>

---

### What we covered so far

```grid(2:2,#1da06e:#183243,gap:.3em)
vue-cli
DevTools
Templates
Components
```

---

### Recommended next steps

```grid(3:2,#1da06e:#1b6959,gap:.1em)
Debugging
Nuxt.js
Vue-Material
Vuex
Animations
Testing
```

---

### Summary

- For developers
- Just try it ;)

---

<div style="text-align: center">
    <h3>Join the revolution</h3>
    <img src="images/chu.jpg" style="width:auto;" />
</div>

---

##### Thanks :-)