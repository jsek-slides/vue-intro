class GridExtension {

    constructor() {
        this.lastId = 0;
    }

    parse(classname) {
        const classPattern = /lang-grid\((?<columns>\d+):(?<rows>\d+),(?<bg>[^,\)]+)?,?(?<rest>[^\)]+)?/u;
        const match = classPattern.exec(classname);
        if (match) {
            return {
                columns: parseInt(match.groups.columns, 10),
                rows: parseInt(match.groups.rows, 10),
                bg: match.groups.bg,
                rest: match.groups.rest,
            };
        } else {
            console.error(`GridExtension: Cannot parse classname: ${classname}`)
        }
    }

    init(element) {
        if (element) {
            this.createGrid(element);
        } else {
            [...document.body.querySelectorAll('[class^="lang-grid"]')]
            .map(element => this.createGrid(element));
        }
        return Promise.resolve();
    }

    createGrid(element) {
        const options = this.parse(element.className);
        if (options) {
            this.replace(element.parentNode, this.render(options, element.innerText));
        }
    }

    replace(element, html) {
        const newDiv = document.createElement('div');
        newDiv.innerHTML = html;
        element.parentNode.replaceChild(newDiv, element);
    }

    parseColor(color) {
        let div = document.createElement('div');
        div.style.color = color;
        document.body.appendChild(div);
        let s = window.getComputedStyle(div);

        let [r,g,b,a] = s.color.match(/\((.*)\)/)[1].split(',').map(x => parseFloat(x,10));
        a = a === undefined ? 1 : a;

        div.remove();

        return [r,g,b,a];
    }

    render({columns, rows, bg, rest}, text) {
        const id = ++this.lastId;
        const cells = text.split(/\n|\|/).map(x => x.trim()).filter(x => x);
        const [bg1, bg2] = bg.split(/:->:|:/);
        let useSpectrum = !!bg.match(/:->:/);
        let bgPaletteCss;

        if (useSpectrum) {
            let start = this.parseColor(bg1);
            let end = this.parseColor(bg2);
            let length = cells.length;
            let spectrum = cells.slice(2).map((_, i) => {
                return [
                    start[0] + Math.round((i+1)*(end[0] - start[0])/length),
                    start[1] + Math.round((i+1)*(end[1] - start[1])/length),
                    start[2] + Math.round((i+1)*(end[2] - start[2])/length),
                    start[3] + Math.round((i+1)*(end[3] - start[3])/length),
                ];
            });

            bgPaletteCss = [start,...spectrum,end]
                .map(([r,g,b,a]) => `rgba(${r},${g},${b},${a})`)
                .map((c, i) => `#grid_${id} p:nth-child(${i+1}) { background: ${c}; border: 3px solid ${c}; }`)
                .join('\n');
        }

        let cellCss = `
            ${/bold/.test(rest) ? `font-weight: bold;` : ''}
            ${/thin/.test(rest) ? `font-weight: 200;` : ''}
            background: ${bg1};
            padding: 1.7em .3em;
            text-align: center;
            display: flex;
            align-items: center;
            justify-content: center;
            border: 3px solid ${bg1};
            ${columns + rows === 2 ? 'text-shadow: 1px 2px 13px rgba(0,0,0,.3);' : ''}`;

        let altCellCss = bg2 === 'invert'
            ? 'filter: invert(100%)'
            : bg2 !== undefined
            ? `background: ${bg2}; border: 3px solid ${bg2};`
            : '';

        let altCellSelector = columns % 2 == 1
            ? `#grid_${id} p:nth-child(even)`
            : [`#grid_${id} p:nth-child(-2n+${columns})`]
                .concat([...Array(rows - 1)].map((_, i) => `#grid_${id} p:nth-child($Math.round({(i+1)*columns}) ~ p:nth-child(-2n)+${(i+2)*columns - (i%2?0:1)})`))
                .filter(x => x)
                .join(',\n');

        let gap = /gap:(?<gap>[^,\)]+)/.test(rest)
            ? /gap:(?<gap>[^,\)]+)/.exec(rest).groups.gap
            : null;

        let gridCss = `display: grid;
            margin: 1em 0;
            grid-template-columns: repeat(${columns},1fr);
            grid-template-rows: repeat(${rows},1fr);`
            + (gap ? `grid-gap:${gap};` : '');

        let funky = !/funky/.test(rest) ? '' : `
            /* Credits:  https://codepen.io/mike-schultz/pen/NgQvGO */
            #grid_${id} {
                --borderWidth: 10px;
                position: relative;
            }
            #grid_${id}:after {
                content: '';
                position: absolute;
                top: calc(-1 * var(--borderWidth));
                left: calc(-1 * var(--borderWidth));
                height: calc(100% + var(--borderWidth) * 2);
                width: calc(100% + var(--borderWidth) * 2);
                background: linear-gradient(60deg, #f79533, #f37055, #ef4e7b, #a166ab, #5073b8, #1098ad, #07b39b, #6fba82);
                z-index: -1;
                animation: animatedgradient 5s ease alternate infinite;
                background-size: 300% 300%;
                border-radius: var(--borderWidth);
            }
            @keyframes animatedgradient {
                0%   { background-position:   0% 50%; }
                50%  { background-position: 100% 50%; }
                100% { background-position:   0% 50%; }
            }
        `;

        let cellHover = funky ? '' : `border: 3px solid yellow !important;`;

        return `
            <style>
                #grid_${id}                    { ${gridCss}    }
                #grid_${id} p                  { ${cellCss}    }
                .talk-mode #grid_${id} p:hover { ${cellHover}  }
                ${altCellSelector}             { ${altCellCss} }
                ${bgPaletteCss || ''}
                ${funky}
            </style>
            <div id="grid_${id}">
            ${cells.map(x => `<p>${x}</p>`).join('\n')}
            </div>
        `;
    }
}

window.grid = new GridExtension();