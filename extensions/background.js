class BackgroundExtension {

    init(element) {
        if (element) {
            this.setBackground(element.querySelector('[alt^="bg:"]'));
        } else {
            [...document.body.querySelectorAll('[alt^="bg:"]')]
            .map(element => this.setBackground(element));
        }
        return Promise.resolve();
    }

    setBackground(element) {
        if (!element) return;
        
        element.parentNode.parentNode.style = `
            height: 100%;
            background-image: url(${element.src});
            background-size: 100% 100%;
            background-repeat: no-repeat;
        `;
        element.style= 'display: none';
    }
}

window.background = new BackgroundExtension();